package pl.adamchodera.sdamaterialdesign.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class DashboardPagerAdapter extends FragmentPagerAdapter {

    public DashboardPagerAdapter(final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int position) {
        if (position == 0) {
            return CardViewFragment.newInstance();
        } else {
            return DashboardFragment.newInstance(position);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return "Fragment " + position;
    }
}
